//
//  CardModel.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 18/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import Foundation
import SwiftUI

public var progressValue: Float = 0.25

struct CardModel: Codable{
    var text:String
    var image:String
    var translate:String
}

class FlipCardModel: ObservableObject {
    @Published var translatedWord: [CardModel] = [
        CardModel(text: "Bed", image: "bed", translate: "Cama"),
        CardModel(text: "Mouse", image: "mouse", translate: "Ratón"),
        CardModel(text: "Plane", image: "plane", translate: "Avion"),
        CardModel(text: "Backpack", image: "backpack", translate: "Mochila"),
        CardModel(text: "Tea", image: "tea", translate: "Té"),
        CardModel(text: "Boat", image: "boat", translate: "Barco"),
        CardModel(text: "Bicycle", image: "bicycle", translate: "Bicicleta"),
        CardModel(text: "Road", image: "road", translate: "Carretera"),
        CardModel(text: "Ballon", image: "ballon", translate: "Globo"),
        CardModel(text: "Trash", image: "trash", translate: "Basura"),
        CardModel(text: "Park", image: "tree", translate: "Parque"),
        CardModel(text: "Dinner", image: "dinner", translate: "Cena"),
        CardModel(text: "Map", image: "map", translate: "Mapa"),
        CardModel(text: "Gift", image: "gift", translate: "Regalo"),
        CardModel(text: "Key", image: "key", translate: "Llave"),
        CardModel(text: "Credit Card", image: "creditcard", translate: "Tarjeta de Credito"),
        CardModel(text: "Passport", image: "passport", translate: "Pasaporte"),
        CardModel(text: "Calendar", image: "calendar", translate: "Calendario"),
        CardModel(text: "Luggage", image: "luggage", translate: "Equipaje"),
        CardModel(text: "Camera", image: "photocamera", translate: "Cámara"),
        CardModel(text: "Panel", image: "panel", translate: "Panel"),
        CardModel(text: "Bell", image: "bell", translate: "Campana"),
        CardModel(text: "Compass", image: "compass", translate: "Brújula"),
        CardModel(text: "Television", image: "television", translate: "Televisión")
    ]
}

extension CardModel {
    
    static var a = Int.random(in: 0..<21)
    static var b = Int.random(in: 0..<21)
    static var albums = [a, b]
    
    static var previewCard1 = FlipCardModel().translatedWord[a]
    static var previewCard2 = FlipCardModel().translatedWord[b]
}

extension CardModel{
    
    static func rand() -> [Int] {
        let indice_a = Int.random(in: 0..<2)
        var indice_b = Int.random(in: 0..<2)
        while indice_a == indice_b {
            indice_b = Int.random(in: 0..<2)
        }
        return [indice_a, indice_b]
    }
    
    static func reloadCard() -> Void {
        a = Int.random(in: 0..<21)
        b = Int.random(in: 0..<21)
        
        while a == b {
            b = Int.random(in: 0..<21)
        }
        
        previewCard1 = FlipCardModel().translatedWord[a]
        previewCard2 = FlipCardModel().translatedWord[b]
        
    }
}
