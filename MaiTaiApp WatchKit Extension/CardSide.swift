//
//  CardSide.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 18/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import SwiftUI

import SwiftUI

extension Card {
    struct Side<Content: View> : View {
                let content: Content

                init(@ViewBuilder content: () -> Content) {
                    self.content = content()
                }
                
                var body: some View{
                    ZStack{
                        RoundedRectangle(cornerRadius: 10).addBorder(Color.gray, width: 2, cornerRadius: 10)
                        .frame(width: 150, height: 150, alignment: .center)
                        .foregroundColor(Color.black)
                        .font(Font.system(size: 40.0, design: .rounded))
                        
                        content

                    }
                    .padding(.all, 10)
                }
    }
}

struct CardSide_Previews: PreviewProvider {
    static var previews: some View {
        Card.Side {
            Text("Side")
        }
    }
}

extension View {
    public func addBorder<S>(_ content: S, width: CGFloat = 1, cornerRadius: CGFloat) -> some View where S : ShapeStyle {
        return overlay(RoundedRectangle(cornerRadius: cornerRadius).strokeBorder(content, lineWidth: width))
    }
}
