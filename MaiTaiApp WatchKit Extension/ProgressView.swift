//
//  ProgressView.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 18/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import SwiftUI

struct ProgressView: View {
    
    var body: some View {
        ZStack {

            VStack {
                ProgressBar(progress: progressValue)
                    .frame(width: 80.0, height: 80.0)
                    .padding(20.0)
                
                Button(action: {
                    WKInterfaceController.reloadRootPageControllers(withNames: ["FC", "SC", "QC"], contexts: [""], orientation: .horizontal, pageIndex: 0)
                }) {
                    Text("Restart")
                    }.onAppear(
                        perform: CardModel.reloadCard
                    )
                    
                .frame(width: 150.0, height: 60.0)
                Spacer()
            }
                
            .padding(.top)
            
        }.onAppear{
            if(progressValue<1){
                self.incrementProgress()
            }else{

            }
        }
        .padding(.top)
    }
    
    func incrementProgress() {
        let randomValue = Float(0.25)
        progressValue += randomValue
    }
}

struct ProgressBar: View {
    var progress: Float = progressValue
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 10.0)
                .opacity(0.3)
                .foregroundColor(Color.gray)
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 10.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(Color.blue)
                .rotationEffect(Angle(degrees: 270.0))
                .animation(.linear)

            Text(String(format: "\(Int((progress * 4))) of 4", min(self.progress, 1)*100))
                .font(.body)
                .bold()
        }
    }
}

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView()
    }
}


