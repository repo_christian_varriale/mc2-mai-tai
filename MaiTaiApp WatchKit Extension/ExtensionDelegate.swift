//
//  ExtensionDelegate.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 15/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import WatchKit
import UserNotifications

class ExtensionDelegate: NSObject, WKExtensionDelegate {
    
    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                print("All set!")
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
        
        let content = UNMutableNotificationContent()
        content.title = "Tap2Flip"
        content.body = "Open The App"
        content.badge = NSNumber(value: 3)
        content.sound = UNNotificationSound.default
        
        //Triggered notification with calendar
        
        _ = Calendar(identifier: .gregorian)
        var DateComp = DateComponents()
        DateComp.setValue(09, for: .hour)
        DateComp.setValue(30, for: .minute)
        
        _ = Calendar(identifier: .gregorian)
        var DateComp2 = DateComponents()
        DateComp2.setValue(10, for: .hour)
        DateComp2.setValue(00, for: .minute)
        
        
        _ = Calendar(identifier: .gregorian)
        var DateComp3 = DateComponents()
        DateComp3.setValue(10, for: .hour)
        DateComp3.setValue(15, for: .minute)
        
        
        let trigger_1 = UNCalendarNotificationTrigger.init(dateMatching: DateComp, repeats: true)
        let trigger_2 = UNCalendarNotificationTrigger.init(dateMatching: DateComp2, repeats: true)
        let trigger_3 = UNCalendarNotificationTrigger.init(dateMatching: DateComp3, repeats: true)
        
        //Notification request
        let request_1 = UNNotificationRequest(identifier: "alarm-morning", content: content, trigger: trigger_1)
        let request_2 = UNNotificationRequest(identifier: "alarm-afternoon", content: content, trigger: trigger_2)
        let request_3 = UNNotificationRequest(identifier: "alarm-evening", content: content, trigger: trigger_3)
        
        
        
        UNUserNotificationCenter.current().add(request_1)
        UNUserNotificationCenter.current().add(request_2)
        UNUserNotificationCenter.current().add(request_3)
        
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["alarm-morning"])
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["alarm-afternoon"])
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["alarm-evening"])
    
        
    }
    
    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    
    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
        
        for task in backgroundTasks {
            // Use a switch statement to check the task type
            switch task {
            case let backgroundTask as WKApplicationRefreshBackgroundTask:
                // Be sure to complete the background task once you’re done.
                backgroundTask.setTaskCompletedWithSnapshot(false)
            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
                // Snapshot tasks have a unique completion call, make sure to set your expiration date
                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
                // Be sure to complete the connectivity task once you’re done.
                connectivityTask.setTaskCompletedWithSnapshot(false)
            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
                // Be sure to complete the URL session task once you’re done.
                urlSessionTask.setTaskCompletedWithSnapshot(false)
            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
                // Be sure to complete the relevant-shortcut task once you're done.
                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
                // Be sure to complete the intent-did-run task once you're done.
                intentDidRunTask.setTaskCompletedWithSnapshot(false)
            default:
                // make sure to complete unhandled task types
                task.setTaskCompletedWithSnapshot(false)
            }
        }
    }
    
}
