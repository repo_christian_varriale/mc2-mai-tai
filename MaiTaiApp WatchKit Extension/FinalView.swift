//
//  FinalView.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 23/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import SwiftUI

struct FinalView: View {
    var body: some View {
        Text("See You Tomorrow !")
    }
}

struct FinalView_Previews: PreviewProvider {
    static var previews: some View {
        FinalView()
    }
}
