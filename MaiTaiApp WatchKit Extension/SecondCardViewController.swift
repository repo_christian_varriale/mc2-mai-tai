//
//  SecondCardController.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 19/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class SecondCardViewController: WKHostingController<Card> {
    override var body: Card {
//        return Card(card: CardModel(text: "MOUSE", image: "mouse", translate: "Topo"))
        
        return Card(card: CardModel.previewCard2)
    }
}
