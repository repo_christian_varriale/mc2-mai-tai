//
//  NotificationView.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 15/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    let card: CardModel
    
    /*
     The NotificationView uses the same FlashCard as the CardList to show a
     flash card in an interactive notification.
     */
    
    var body: some View {
        
        VStack {
            Text("Guess this card?")
                .font(.body)
                .multilineTextAlignment(.center)
                .padding(.bottom, 10)
            Card(card: card)
                .padding(.bottom, 7)
        }
        
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView(card: CardModel.previewCard1)
    }
}
