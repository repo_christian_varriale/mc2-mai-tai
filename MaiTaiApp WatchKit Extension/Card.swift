//
//  Card.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 18/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import SwiftUI

struct Card: View {
        let card: CardModel
        @State private var half = false
        @State private var dim = false

            var body: some View {
                FlipView(
                    
                    Side {
                        
                        VStack{
                            Image(card.image)
                                .resizable()
                                .frame(width: 85.0, height: 85.0)
                                .scaledToFit()
                                .padding(.top, 20)

                                .scaleEffect(half ? 1.0 : 0.5)
                                .opacity(dim ? 1.0 : 0.2)
                                .animation(
                                    Animation.easeOut(duration: 1.0)
                                    .repeatForever())
                                
                                .onAppear {
                                    self.dim.toggle()
                                    self.half.toggle()
                            }
                            
                            Text(card.text)
                                .font(Font.system(size: 30.0, design: .rounded))
                                .multilineTextAlignment(.center)
                                .padding(.bottom, 20)
                        }
                        
                    },
                    Side {
                        Text(card.translate)
                            .font(Font.system(size: 30.0, design: .rounded))
                            .multilineTextAlignment(.center)
                    }
                )
            }
}

struct Card_Previews: PreviewProvider {
    static var previews: some View {
        Card(card: CardModel.previewCard1)
    }
}

