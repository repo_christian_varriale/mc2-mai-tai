//
//  FinalViewController.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 23/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class FinalViewController: WKHostingController<FinalView> {
    override var body: FinalView {     
        return FinalView()
    }
}
