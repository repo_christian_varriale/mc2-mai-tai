//
//  QuizViewController.swift
//  MaiTaiApp WatchKit Extension
//
//  Created by Christian Varriale on 19/01/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class QuizViewController: WKHostingController<QuizView> {
    override var body: QuizView {
        return QuizView()
    }
}
